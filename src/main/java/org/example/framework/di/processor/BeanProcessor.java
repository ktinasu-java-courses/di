package org.example.framework.di.processor;

@FunctionalInterface
public interface BeanProcessor {
    Object process(Object bean, Class<?> originalClazz, Class<?>[] argumentTypes, Object[] arguments);
}
