package org.example.framework.di;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.di.annotation.Autowired;
import org.example.framework.di.annotation.Component;
import org.example.framework.di.exception.*;
import org.example.framework.di.processor.BeanProcessor;
import org.reflections.Reflections;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;


@Slf4j
public class Container {
    private final Map<String, Class<?>> definitions = new HashMap<>();
    private final Map<String, Object> beans = new HashMap<>();
    private final List<BeanProcessor> processors = new ArrayList<>();
    private final AtomicBoolean wired = new AtomicBoolean(false);

    public synchronized void register(String prefix) {
        final Reflections reflections = new Reflections(prefix);
        final Set<Class<?>> clazzez = reflections.getTypesAnnotatedWith(Component.class);
        log.debug("classes: {}", clazzez);
        clazzez.forEach(o -> definitions.put(o.getName(), o));
    }

    public void register(final Class<?> clazz) {
        register(clazz.getName(), clazz);
    }

    public void register(final Object bean) {
        register(bean.getClass().getName(), bean);
    }

    public synchronized void register(final String name, final Class<?> clazz) {
        assertNotWired();
        definitions.put(name, clazz);
    }

    public synchronized void register(final String name, final Object bean) {
        assertNotWired();
        final Class<?> clazz = bean.getClass();
        if (bean instanceof BeanProcessor) {
            processors.add((BeanProcessor) bean);
        }
        definitions.put(name, clazz);
        beans.put(name, bean);
    }

    public synchronized void wire() {
        assertNotWired();
        wired.set(true);

        doWire();

        final List<Object> autowireBeans = getBeansByAnnotation(Autowired.class);
        for (final Object autowireBean : autowireBeans) {
            final List<Method> methods = Arrays.stream(autowireBean.getClass().getDeclaredMethods())
                    .filter(o -> o.isAnnotationPresent(Autowired.class))
                    .collect(Collectors.toList());
            for (final Method method : methods) {
                final Parameter[] parameters = method.getParameters();
                for (final Parameter parameter : parameters) {
                    if (List.class.isAssignableFrom(parameter.getType())) {
                        final ParameterizedType parameterizedType = (ParameterizedType) parameter.getParameterizedType();
                        final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                        if (actualTypeArguments.length != 1) {
                            continue;
                        }

                        final List<Object> beans = getBeansByType((Class<Object>) actualTypeArguments[0]);
                        try {
                            method.invoke(autowireBean, beans);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            throw new AutowireException(e);
                        }
                    }
                }
            }
        }
    }

    public synchronized Object getBean(final String name) {
        assertWired();
        return Optional.ofNullable(beans.get(name)).orElseThrow(() -> new BeanNotFoundException(name));
    }

    public <T> T getBean(final String name, final Class<T> clazz) {
        return (T) getBean(name);
    }

    public <T> T getBean(final Class<T> clazz) {
        final long count;
        synchronized (this) {
            count = beans.values().stream().filter(o -> o.getClass().isAssignableFrom(clazz)).count();
        }
        if (count > 1) {
            throw new InvalidContainerStateException("more than one assignableFrom");
        }
        return getBean(clazz.getName(), clazz);
    }

    public synchronized List<Object> getBeansByAnnotation(Class<? extends Annotation> annotationClazz) {
        assertWired();
        return beans.values().stream()
                .filter(o -> o.getClass().isAnnotationPresent(annotationClazz))
                .collect(Collectors.toList());
    }

    public synchronized <T> List<T> getBeansByType(final Class<T> typeClazz) {
        assertWired();
        return beans.values().stream()
                .filter(o -> typeClazz.isAssignableFrom(o.getClass()))
                .map(o -> (T) o)
                .collect(Collectors.toList());
    }

    private synchronized void doWire() {
        while (true) {
            int count = 0;

            for (final Map.Entry<String, Class<?>> entry : definitions.entrySet()) {
                final String name = entry.getKey();
                final Class<?> clazz = entry.getValue();
                log.debug("try to instantiate: {}", name);
                Object bean = nextGeneration(clazz);
                if (bean == null) {
                    continue;
                }

                log.debug("bean created: {}, {}", name, clazz.getName());
                final Class<?> originalClazz = bean.getClass();
                final Constructor<?>[] beanClassConstructors = bean.getClass().getConstructors();
                final Constructor<?> beanClassConstructor = beanClassConstructors[0];
                final Class<?>[] argumentTypes = Arrays.stream(beanClassConstructor.getParameters())
                        .map(Parameter::getType).toArray(Class<?>[]::new);
                final Object[] arguments = Arrays.stream(beanClassConstructor.getParameters())
                        .map(o -> instantiate(o).orElseThrow(RuntimeException::new))
                        .toArray();
                for (BeanProcessor processor : processors) {
                    bean = processor.process(bean, originalClazz, argumentTypes, arguments);
                }
                beans.put(name, bean);
                count++;
            }
            log.debug("after next generation: {}", beans.keySet());

            if (definitions.size() == beans.size()) {
                return;
            }

            if (count == 0) {
                throw new UnmetDependenciesException();
            }
        }
    }

    private void assertWired() {
        if (!wired.get()) {
            throw new InvalidContainerStateException("not wired");
        }
    }

    private void assertNotWired() {
        if (wired.get()) {
            throw new InvalidContainerStateException("already wired");
        }
    }

    private Optional<?> instantiate(final Parameter parameter) {
        final Class<?> paramClazz = parameter.getType();
        synchronized (this) {
            return beans.values().stream()
                    .filter(o -> paramClazz.isAssignableFrom(o.getClass()))
                    .findFirst();
        }
    }

    private synchronized boolean isInstantiated(Class<?> clazz) {
        return beans.values().stream()
                .anyMatch(o -> clazz.equals(o.getClass()));
    }

    private Object nextGeneration(final Class<?> clazz) {
        if (isInstantiated(clazz)) {
            return null;
        }

        final Constructor<?>[] constructors = clazz.getConstructors();
        if (constructors.length != 1) {
            try {
                constructors[0] = clazz.getConstructor();
            } catch (NoSuchMethodException e) {
                throw new AmbigiousConstructorException("bean must have only one public constructor or default constructor: " + clazz.getName(), e);
            }
        }

        final Constructor<?> constructor = constructors[0];
        final Parameter[] parameters = constructor.getParameters();
        final List<?> args = Arrays.stream(parameters)
                .filter(o -> instantiate(o).isPresent())
                .map(o -> instantiate(o).get())
                .collect(Collectors.toList());
        if (args.size()!=parameters.length) {
            return null;
        }
        try {
            return constructor.newInstance(args.toArray());
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new BeanInstantiationException(e);
        }
    }
}
