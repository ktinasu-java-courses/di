package org.example.framework.di.exception;

public class InvalidContainerStateException extends RuntimeException {
  public InvalidContainerStateException() {
  }

  public InvalidContainerStateException(String message) {
    super(message);
  }

  public InvalidContainerStateException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidContainerStateException(Throwable cause) {
    super(cause);
  }

  public InvalidContainerStateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
