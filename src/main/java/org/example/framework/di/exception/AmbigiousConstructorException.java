package org.example.framework.di.exception;

public class AmbigiousConstructorException extends RuntimeException {
  public AmbigiousConstructorException() {
  }

  public AmbigiousConstructorException(String message) {
    super(message);
  }

  public AmbigiousConstructorException(String message, Throwable cause) {
    super(message, cause);
  }

  public AmbigiousConstructorException(Throwable cause) {
    super(cause);
  }

  public AmbigiousConstructorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
